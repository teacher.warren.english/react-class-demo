import { composeWithDevTools } from "@redux-devtools/extension"
import { applyMiddleware, combineReducers, createStore } from "redux"
import { countMiddleware } from "./middleware"
import { countReducer } from "./reducers"

const appReducer = combineReducers({
    count: countReducer
})

const middleware = applyMiddleware(countMiddleware)

const store = createStore(appReducer, composeWithDevTools(middleware))

export default store