import { ACTION_COUNT_BOOST, ACTION_COUNT_DECREMENT, ACTION_COUNT_INCREMENT } from "./actions";

export function countReducer(state = 0, action) {
    switch (action.type) {
        case ACTION_COUNT_INCREMENT:
            return state + 1
        case ACTION_COUNT_DECREMENT:
            return state - 1
        case ACTION_COUNT_BOOST:
            return state + action.payload

        default:
            return state
    }
}