import { ACTION_COUNT_SUPERBOOST, boost } from "./actions"

export const countMiddleware = ({ dispatch }) => next => action => {
    next(action)

    // logic to do stuff
    if (action.type === ACTION_COUNT_SUPERBOOST) {
        console.log("Charging the Superboost...");
        setTimeout(() => {
            console.log("pew pew pew");
            dispatch(boost(20))

        }, 2000);
    }
}