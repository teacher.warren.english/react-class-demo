// Action Labels
export const ACTION_COUNT_INCREMENT = '[count] INCREMENT'
export const ACTION_COUNT_DECREMENT = '[count] DECREMENT'
export const ACTION_COUNT_BOOST = '[count] BOOST'
export const ACTION_COUNT_SUPERBOOST = '[count] SUPERBOOST'

export const increment = () => ({
    type: ACTION_COUNT_INCREMENT
})

export const decrement = () => ({
    type: ACTION_COUNT_DECREMENT
})

export const boost = (amount = 1) => ({
    type: ACTION_COUNT_BOOST,
    payload: amount
})

export const superboost = () => ({
    type: ACTION_COUNT_SUPERBOOST,
})