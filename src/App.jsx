import { useEffect } from 'react';
import { useContext } from 'react';
import './App.css';
import ProfileCard from './Components/ProfileCard';
import SimpleForm from './Components/SimpleForm';
import NotFound from './Components/NotFound'
import { ProfileContext } from './HOC/ProfileContext';
import { BrowserRouter, Routes, Route, NavLink } from 'react-router-dom'
import PicturePage from './Components/PicturePage';
import CounterPage from './Components/CounterPage';
// import GuitarInfo from './Components/GuitarInfo';
// import UserProfile from './Components/UserProfile';

function App() {

  const [, setProfile] = useContext(ProfileContext)

  useEffect(
    () => async () => {
      fetch("https://randomuser.me/api")
        .then(response => response.json())
        .then(jsonData => jsonData.results[0])
        .then(data => setProfile({
          username: data.login.username,
          picture: data.picture.thumbnail,
        }))
        .catch(error => console.error(error.message))
    }, [setProfile]
  )

  const guitar = {
    manufacturer: "Fender",
    model: "Strat",
  }

  return (
    <BrowserRouter>
      <div className="App">
        <h1>My React App</h1>
        <Routes>
          <Route path='/profile' element={<ProfileCard />} />
          <Route path='/form' element={<SimpleForm />} />
          <Route path='/fruit' element={<PicturePage />} />
          <Route path='/counter' element={<CounterPage />} />
          <Route path='*' element={<NotFound />} />
        </Routes>
        <footer>
          <h4>Navigation:</h4>
          <nav>
            <NavLink to={'/profile'}>Go to Profile</NavLink>
            <NavLink to={'/form'}>Go to Form</NavLink>
            <NavLink to={'/fruit'}>Go to Fruit Pictures</NavLink>
            <NavLink to={'/counter'}>Go to Count Page</NavLink>
          </nav>
        </footer>
        {/* <UserProfile/> */
      /* <GuitarInfo/> 
      <p>
        {`${guitar.manufacturer} ${guitar.model}`}
      </p>*/}
      </div>
    </BrowserRouter>
  );
}

export default App;
