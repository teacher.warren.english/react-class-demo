function GuitarInfo() {
    const guitars = [
        "Fender Strat",
        "Gibson Les Paul",
        "Ibanez Explorer",
    ]

    return (
        <>
            {/* Div pollution */}
            <h1 className="Title">This is my GuitarInfo component</h1>
            <fieldset>
                <label htmlFor="name">Username</label>
                <input id="name" type="text" placeholder="johndoe" />
            </fieldset>

            <ul>{guitars.map((g, index) => <li key={index}>{g}</li>)}</ul>
        </>
    )
}

export default GuitarInfo