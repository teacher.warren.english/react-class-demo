function UserProfileCard(props) {

    const {picture, username, country, name} = props.userdata

    return (
        <>
            <img src={picture} alt="profile" />
            <p>{username}</p>
            <p>{country}</p>
            <p>{name}</p>
        </>
    )
}

export default UserProfileCard