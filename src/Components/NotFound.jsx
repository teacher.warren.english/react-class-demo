function NotFound() {
    return (
        <p>What you're looking for does not exist!</p>
    )
}

export default NotFound