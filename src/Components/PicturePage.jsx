import { getPublicImagePath, getSrcImagePath } from "../images/imageMap"

function PicturePage() {
    const imagesToRender = ['1', '2', '3', '2']
        
    return (

        <>
            <h3>My Pictures</h3>
            <div>
                <h5>Src Images</h5>
                {
                    imagesToRender.map( (item, index) => {
                        return (
                            <img src={ getSrcImagePath(item)} alt={`fruit${index}`} key={index} />
                        )
                    })
                }
            </div>
            <div>
                <h5>Public Images</h5>
                {
                    imagesToRender.map((item, index) => {
                        return (
                            <img src={getPublicImagePath(item)} alt={`fruit${index}`} key={index}/>
                        )
                    })
                }
            </div>
        </>
    )
}

export default PicturePage