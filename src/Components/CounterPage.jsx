import { useDispatch, useSelector } from "react-redux"
import { boost, decrement, increment, superboost } from "../ReduxParts/actions"
import { getCount, getCountPositive } from "../ReduxParts/selectors"

function CounterPage() {

    const count = useSelector(getCount)
    const countPositive = useSelector(getCountPositive)

    const dispatch = useDispatch()

    // Button Handlers
    const handleIncrement = () => dispatch(increment())
    const handleDecrement = () => dispatch(decrement())
    const handleBoost = () => dispatch(boost(5))
    const handleSuperboost = () => dispatch(superboost())

    return (
        <>
            <h3>Counter: { count }</h3>
            <h4>The current count is {countPositive ? 'Positive' : 'Negative'}</h4>
            <button onClick={handleIncrement}>Increase 👆</button>
            <button onClick={handleDecrement}>Decrease 👇</button>
            <button onClick={handleBoost}>Boost 🚀</button>
            <button onClick={handleSuperboost}>Super Boost! ⚛</button>
        </>
    )
}

export default CounterPage