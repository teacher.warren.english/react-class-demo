import { useContext } from "react"
import { ProfileContext } from "../HOC/ProfileContext"

function ProfileCard() {
    const [profile, setProfile] = useContext(ProfileContext)

    function handleLogout() {
        setProfile(null)
    }

    return (
        <>
            {
                profile &&
                <div>
                    <img src={profile.picture} alt="profile" />
                    <p>{profile.username}</p>
                    <button onClick={handleLogout}>Logout</button>
                </div>
            }
        </>
    )
}

export default ProfileCard