import { useEffect, useState } from "react"
import UserProfileCard from "./UserProfileCard"

function UserProfile() {
    // Hooks
    const [user, setUser] = useState({})
    const [data, setData] = useState({})

    useEffect(() => {
        const fetchUser = async () => {
            fetch("https://randomuser.me/api")
                .then(response => {
                    if (response.ok) {
                        console.log(response);
                        return response.json()
                    }
                })
                .then(data => {
                    console.log(data);
                    const dataResults = data.results[0]
                    const _userData = {
                        username: dataResults.login.username,
                        country: dataResults.location.country,
                        name: `${dataResults.name.title} ${dataResults.name.first} ${dataResults.name.last}`,
                        picture: dataResults.picture.thumbnail,
                    }
                    setData(_userData)
                })
                .catch(error => {
                    console.error(error.message)
                })
        }
        fetchUser()
    }, [user]) // run once (no dependencies)

    function handleButtonClick() {
        console.log("Changing username")
        // user.username = "warren-west"
        setUser({ username: "warrenwest" })
    }

    return (
        <>
            <p>User Data:</p>
            {user.username ? <p>{user.username}</p> : <p>No user data found.</p>}
            <button onClick={handleButtonClick}>Change username</button>
            <UserProfileCard userdata={data} />
        </>
    )
}

export default UserProfile