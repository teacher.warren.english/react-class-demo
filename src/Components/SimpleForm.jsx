import { useForm } from 'react-hook-form'

function SimpleForm() {
    const { register, handleSubmit, formState: { errors } } = useForm()

    function handleOnSubmit(data) {
        console.log(data);
    }

    return (
        <>
            <form onSubmit={handleSubmit(handleOnSubmit)}>
                <input type="text" placeholder='Sweden'
                    {...register('nationality', {required: 'Nationality is required'})} /> {/* Nationality */}
                <input type="text" placeholder='John'
                    {...register('firstname', {minLength: {value: 2, message: "First name must be at least 2 characters"}, required: 'First name is required'})} /> {/* First Name */}
                <input type="text" placeholder='Doe'
                    {...register('lastname', {minLength: 2})} /> {/* Last Name */}
                <button type='submit'>Submit</button>
                {errors.nationality && <p>{errors.nationality.message}</p>}
                {errors.firstname && <p>{errors.firstname.message}</p>}
            </form>
        </>
    )
}

export default SimpleForm