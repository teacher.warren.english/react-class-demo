import banana from './a.png'
import strawberry from './b.png'
import apple from './c.jpg'

const getImagesFromSrc = {
    1 : banana,
    2 : strawberry,
    3 : apple,
}

const getImagesFromPublic = {
    1 : '/img/a.png',
    2 : '/img/b.png',
    3 : '/img/c.jpg',
}

export const getSrcImagePath = imageName => getImagesFromSrc[imageName]

export const getPublicImagePath = imageName => getImagesFromPublic[imageName]